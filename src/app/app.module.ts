import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { Network } from '@ionic-native/network';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Plans } from '../mocks/providers/plans';
import { PlansPage } from '../pages/plans/plans';
import { Api, Settings, User,TrainerProvider,QuizProvider,LoadingProvider } from '../providers';
import { MyApp } from './app.component';
import { Globals } from './globals';
import { NgCalendarModule  } from 'ionic2-calendar';
import { ContentPage } from '../pages/content/content';
import { TrainerPage } from '../pages/trainer/trainer';
import { LoginPage } from '../pages/login/login';
import { LoginPageModule } from '../pages/login/login.module';
import { DetailtrainerPage } from '../pages/detailtrainer/detailtrainer';
import { PlansPageModule } from '../pages/plans/plans.module';
import { TrainerPageModule } from '../pages/trainer/trainer.module';
import { QuizPage } from '../pages/quiz/quiz';
import { QuizPageModule } from '../pages/quiz/quiz.module';
import { QuizsPageModule } from '../pages/quizs/quizs.module';
import { AlertProvider } from '../providers/alert/alert';
import { IonSimpleWizardStep } from '../pages/ion-simple-wizard/ion-simple-wizard.step.component';
import { IonSimpleWizard } from '../pages/ion-simple-wizard/ion-simple-wizard.component';
import { SignupPage } from '../pages/signup/signup';
import { ProfilePageModule } from '../pages/profile/profile.module';











// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp,
    DetailtrainerPage,
    IonSimpleWizard,
    IonSimpleWizardStep,
    SignupPage,
    QuizPage
  ],
  imports: [ 
    NgCalendarModule,
    FormsModule, 
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoginPageModule,
    PlansPageModule,
    TrainerPageModule,
    QuizsPageModule,
    ProfilePageModule,



    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PlansPage,
    TrainerPage,
    LoginPage,
    DetailtrainerPage,
    QuizPage,
    SignupPage,
  

  ],
  providers: [
    Api,
    Plans,
    User,
    Camera,
    SplashScreen,
    StatusBar,
    Globals,
    TrainerProvider,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    QuizProvider,
    LoadingProvider,
    AlertProvider,
    Network,
  ],
  
})
export class AppModule { }



// WEBPACK FOOTER //
// ./src/app/app.module.ts