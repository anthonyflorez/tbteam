import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';
import { PlansPage } from '../pages/plans/plans'
import { Network } from '@ionic-native/network';


import { FirstRunPage } from '../pages';
import { Settings, AlertProvider } from '../providers';
// import { MbscEventcalendarOptions } from '@mobiscroll/angular';}
import { ContentPage } from '../pages/content/content';
import { TrainerPage } from '../pages/trainer/trainer';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';
import { QuizPage } from '../pages/quiz/quiz';
import { QuizsPage } from '../pages/quizs/quizs';
import { ProfilePage } from '../pages/profile/profile';




let now = new Date();
@Component({
    templateUrl: `app.html`
})

export class MyApp {
    pages: Array<{ title: string, component: any, img: string }>;
    id: any


    testMethod() {
        console.log('console.log test');
    }

    rootPage = FirstRunPage;

    @ViewChild(Nav) nav: Nav;



    constructor(private translate: TranslateService, private platform: Platform, settings: Settings, private config: Config,
        private statusBar: StatusBar, private splashScreen: SplashScreen, public storage: Storage, private network: Network,private alert:AlertProvider) {



        this.storage.get('people_id').then(data => {
            console.log(data)
            this.id = data;
            if (data == undefined) {

                console.log("no esta definido")
            } else {
                console.log("si esta definido")
            }
        })
        // watch network for a disconnect



        this.pages = [
            { title: 'Entrenamientos', component: TrainerPage, img: 'assets/img/icono-p14-80.png' },
            { title: 'Encuestas', component: QuizPage, img: 'assets/img/icono-p14-80.png' },
            { title: 'Perfíl', component: ProfilePage, img: 'assets/img/icono-p14-80.png' },
            { title: 'Salir', component: LoginPage, img: 'assets/img/icono-p14-80.png' },
        ];
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.


            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
        this.initTranslate();
        this.platform.ready().then(() => {
            this.network.onConnect().subscribe(data => {
              this.alert.presentAlert('Aviso','Ya estas conectado, puedes usar la aplicación','Aceptar');
            }, error => console.error(error));
      
            this.network.onDisconnect().subscribe(data => {
              this.nav.setRoot(LoginPage)
              this.alert.presentAlert('Aviso','Para usar esta aplicacion debes estar conectado a internet','Aceptar');
            }, error => console.error(error));
          });

    }


    initTranslate() {
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang('es');
        const browserLang = this.translate.getBrowserLang();

        if (browserLang) {
            if (browserLang === 'es') {
                const browserCultureLang = this.translate.getBrowserCultureLang();

                if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
                    this.translate.use('zh-cmn-Hans');
                } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
                    this.translate.use('zh-cmn-Hant');
                }
            } else {
                this.translate.use(this.translate.getBrowserLang());
            }
        } else {
            this.translate.use('ar'); // Set your language here
        }

        this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
            this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.title == "Salir") {
            this.storage.remove('id_user');

        }
        this.nav.setRoot(page.component);

    }
}



// WEBPACK FOOTER //
// ./src/app/app.component.ts