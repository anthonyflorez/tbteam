import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class Globals {
  public theme:string = 'ios';
  public lang:string = 'en';

  private _subject = new Subject<any>();


  newEvent(event) {
    this._subject.next(event);
  }

  get events$ () {
    return this._subject.asObservable();
  }

}
