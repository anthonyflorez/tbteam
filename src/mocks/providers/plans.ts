import { Injectable } from '@angular/core';

import { Plan } from '../../models/plan';

@Injectable()
export class Plans {
  plans: Plan[] = [];

  defaultPlan: any = {
    "name": "Burt Bear",
    "profilePic": "assets/img/speakers/bear.jpg",
    "about": "Burt is a Bear.",
  };


  constructor() {
    let plans = [
      {
        "id":"1",
        "name": "Agua",
        "profilePic": "assets/img/speakers/agua.jpg",
        "about": ""
      },
      {
        "id":"2",
        "name": "Tierra",
        "profilePic": "assets/img/speakers/tierra.jpg",
        "about": ""
      },
      {
        "id":"3",
        "name": "Mental",
        "profilePic": "assets/img/speakers/mental.jpg",
        "about": ""
      }
      
    ];

    for (let plan of plans) {
      this.plans.push(plan);
    }
  }



  query(params?: any) {
    if (!params) {
      return this.plans;
    }

    return this.plans.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(plan: Plan) {
    this.plans.push(plan);
  }

  delete(plan: Plan) {
    this.plans.splice(this.plans.indexOf(plan), 1);
  }
}
