import { Directive, ElementRef, Input, HostListener, Renderer2, ViewChild } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { NgControl } from '@angular/forms';
import { ToastController } from "ionic-angular";
import { User } from "../../providers/user/user"
/**
 * Generated class for the DetectFocusDirective directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[detect-focus]' // Attribute selector
})
export class DetectFocusDirective {
  // @ViewChild('document', { read: ElementRef })
  // private document: ElementRef;

  constructor(private el: ElementRef, private http: Http, public control: NgControl, public renderer: Renderer2, private toastCtrl: ToastController,public user: User,) {
    // console.log(this.document.nativeElement)
  }
  @ViewChild('document', { read: ElementRef })
  @Input() typeField: string;
  @Input() datainput:string;
  @Input() typeDocument: number;
  @ViewChild("myButton") myButton: ElementRef;
  public response:any;

  @HostListener('blur') onblur(){
      this.validate(this.datainput);
  }

  private validate(data)
  {
    let element = this.el.nativeElement;
    // console.log(element)
    this.user.validatedocument(data).subscribe(data=>{
      // this.renderer.addClass(this.myButton.nativeElement, "my-class");
      this.response= data;
      if(this.response.data.exist){
        this.renderer.addClass(this.el.nativeElement, 'invalid');
        this.renderer.removeClass(this.el.nativeElement, 'ng-valid');
        this.renderer.addClass(this.el.nativeElement, 'ng-invalid');    
      }else{
        this.renderer.removeClass(this.el.nativeElement, 'invalid');
      }

      
    })

    
    // console.log("en la funcion")

    // console.log(element.val)
    // console.log(element)
    // if(element.value != "")
    // {
    //   let headers = new Headers();
    //   headers.append('Content-Type','application/json');
    //   let requestOptions = new RequestOptions({ headers: headers });
    //   if(this.typeField == "email")
    //   {
    //     if(this.regexEmail(element.value) == true)
    //     {
    //       let headers= new Headers();
    //       headers.append('Content-type', 'application/json');
    //       return new Promise(resolve=>{
    //         this.http.post("http://metroredapp.hmetro.med.ec/metrored/public/auth/validateEmail",{"usuario":element.value}, {headers})
    //         .map(res => res.json())
    //         .subscribe(data => {
    //             if(data.response == false)
    //             {
    //               this.renderer.setElementClass(this.el.nativeElement,'invalid',true);
    //               let toast = this.toastCtrl.create({
    //                     message: "El correo ya esta en uso",
    //                     duration: 1500
    //               })
    //               toast.present();
    //             }
    //             else
    //             {
    //               this.renderer.setElementClass(this.el.nativeElement,'invalid',false);
    //             }
    //         })
    //       })
    //     }
    //   }
    //   else
    //   {
    //     if(this.typeDocument == 2){
    //       this.renderer.setElementClass(this.el.nativeElement, 'invalid', false);
    //     }
    //     else{
    //       if(this.validateDocumentoEcuador(element.value)){
    //         let headers= new Headers();
    //         headers.append('Content-type', 'application/json');
    //         return new Promise(resolve=>{
    //           this.http.post("http://metroredapp.hmetro.med.ec/metrored/public/auth/validateCedula",{"cedula":element.value}, {headers})
    //           .map(res => res.json())
    //           .subscribe(data => {
    //               if(data.response == false)
    //               {
    //                 this.renderer.setElementClass(this.el.nativeElement,'invalid',true);
    //                 let toast = this.toastCtrl.create({
    //                       message: "La cedula ya esta en uso",
    //                       duration: 1500
    //                 })
    //                 toast.present();
    //               }
    //               else
    //               {
    //                 this.renderer.setElementClass(this.el.nativeElement,'invalid',false);
    //               }
    //           })
    //         })
    //       }else{
            // this.renderer.setElementClass(this.el.nativeElement, 'invalid', true);
            // let toast = this.toastCtrl.create({
            //   message: 'Formato de cedula incorrecta',
            //   duration: 1500,
            //   position: 'top'
            // })
    //         toast.present()
    //       }
    //     }
    //   }
    // }
  }

  private regexEmail(input)
  {
    var EMAIL_REGEX = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if(EMAIL_REGEX.test(input))
      {
        return true;
      }
      else
      {
        return false;
      }
  }
  
  private regexNumber(input)
  {
    var NUMBER_REGEX = /^[0-9]+$/;
      if(NUMBER_REGEX.test(input))
      {
        return true;
      }
      else
      {
        return false;
      }
  }

  private validateDocumentoEcuador(input){
    if(input.length == 10){
      let region = input.substring(0, 2);
      if(region >= 1 && region <= 24){
        let ultimo_digito = input.substring(9, 10);

        let pares = parseInt(input.substring(1, 2)) + parseInt(input.substring(3, 4)) + parseInt(input.substring(5, 6)) + parseInt(input.substring(7, 8));

        let numero1 = input.substring(0, 1)
        numero1 = numero1 * 2
        if(numero1 > 9){
          numero1 = numero1 - 9
        }
        let numero3 = input.substring(2, 3)
        numero3 = numero3 * 2
        if(numero3 > 9){
          numero3 = numero3 - 9
        }
        let numero5 = input.substring(4, 5)
        numero5 = numero5 * 2
        if(numero5 > 9){
          numero5 = numero5 - 9
        }
        let numero7 = input.substring(6, 7)
        numero7 = numero7 * 2
        if(numero7 > 9){
          numero7 = numero7 - 9
        }
        let numero9 = input.substring(8, 9)
        numero9 = numero9 * 2
        if(numero9 > 9){
          numero9 = numero9 - 9
        }
        let impares = numero1 + numero3 + numero5 + numero7 + numero9
        let suma_total = (pares + impares)

        let primer_digito = String(suma_total).substring(0, 1)

        let decena = (parseInt(primer_digito) + 1) * 10

        let digito_validador = decena - suma_total

        if(digito_validador == 10){
          digito_validador = 0;
        }
        if(digito_validador == ultimo_digito){
           return true;
        }
        else{
          return false;
        }
      }
    }
  }
}