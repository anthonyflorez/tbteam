import { NgModule } from '@angular/core';
import { ExampleDirective } from './example/example';
@NgModule({
	declarations: [ExampleDirective],
	imports: [],
	exports: [ExampleDirective]
})
export class DirectivesModule {}
