import { Directive } from '@angular/core';

/**
 * Generated class for the ExampleDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[example]' // Attribute selector
})
export class ExampleDirective {

  constructor() {
    console.log('Hello ExampleDirective Directive');
  }

}
