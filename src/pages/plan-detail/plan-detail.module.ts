import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { PlanDetailPage } from './plan-detail';

@NgModule({
  declarations: [
    PlanDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    PlanDetailPage
  ]
})
export class PlanDetailPageModule { }
