import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Plan } from '../../models/plan';
import { Plans } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  currentPlans: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public plans: Plans) { }

  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentPlans = [];
      return;
    }
    this.currentPlans= this.plans.query({
      name: val
    });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openPlan(plan: Plan) {
    this.navCtrl.push('ItemDetailPage', {
      plan: plan
    });
  }

}
