import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';

import { Plan } from '../../models/plan';
import { Plans } from '../../mocks/providers/plans';

@IonicPage()
@Component({
  selector: 'page-plans',
  templateUrl: 'plans.html'
})
export class PlansPage {
  currentPlans: Plan[];

  constructor(public navCtrl: NavController, public plans: Plans, public modalCtrl: ModalController) {
    this.currentPlans = this.plans.query();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addPlan() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(plan => {
      if (plan) {
        this.plans.add(plan);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deletePlan(plan) {
    this.plans.delete(plan);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openPlan(plan: Plan) {
    this.navCtrl.push('PlanDetailPage', {
      plan: plan
    });
  }
}
