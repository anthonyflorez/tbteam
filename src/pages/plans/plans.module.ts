import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { PlansPage } from './plans';

@NgModule({
  declarations: [
    PlansPage,
  ],
  imports: [
    IonicPageModule.forChild(PlansPage),
    TranslateModule.forChild()
  ],
  exports: [
    PlansPage
  ]
})
export class PlansPageModule { }
