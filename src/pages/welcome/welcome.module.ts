import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { WelcomePage } from './welcome';
import { LoginPage } from '../login/login';

@NgModule({
  declarations: [
    WelcomePage,
    LoginPage
  ],
  imports: [
    IonicPageModule.forChild(WelcomePage),
    // IonicPageModule.forChild(LoginPage),
    TranslateModule.forChild()
  ],
  exports: [
    WelcomePage,

  ]
})
export class WelcomePageModule { }
