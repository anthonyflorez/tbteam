import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ContentPage } from './content';
import { FormsModule } from '@angular/forms';
import * as moment from 'moment';
import { NgCalendarModule  } from 'ionic2-calendar';


@NgModule({
  declarations: [
    ContentPage,
  ],
  imports: [
    IonicPageModule.forChild(ContentPage),
    TranslateModule.forChild(),
    NgCalendarModule,
    FormsModule
  ],
  exports: [
    ContentPage
  ]
})
export class ContentPageModule { }
