import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { QuizProvider } from '../../providers';
import { Storage } from '@ionic/storage';
import { TrainerPage } from '../trainer/trainer';
import { LoadingProvider } from '../../providers';

/**
 * Generated class for the QuizPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html',
})
export class QuizPage {
  response: any
  quiz: any
  questions: any
  is_created: boolean
  is_charge_info: boolean
  // Our translated text strings
  private signupErrorString: string;
  submitAttempt: boolean = false;
  step: any;
  stepCondition: any;
  
  stepDefaultCondition: any;
  currentStep: any;
  text_area_quantity_space:number
  is_step: boolean = false;
  @ViewChild('myInput') myInput: ElementRef;
  constructor(public navCtrl: NavController, public navParams: NavParams, public quizprovider: QuizProvider, private storage: Storage, public loading: LoadingProvider, public evts: Events) {
    console.log("*************")
    console.log(window.screen.width)
  
    console.log("*************")

    if(window.screen.width>=360){
      this.text_area_quantity_space=35;
    }else if(window.screen.width >= 361 && window.screen.width <=767){
      this.text_area_quantity_space=80;
    }

    // this.renderer.addClass(this.valid.nativeElement, "my-class");

    /**
   * Step Wizard Settings
   */
    this.step = 1;//The value of the first step, always 1
    this.stepCondition = false;//Set to true if you don't need condition in every step
    this.stepDefaultCondition = this.stepCondition;//Save the default condition for every step
    //You can subscribe to the Event 'step:changed' to handle the current step
    this.evts.subscribe('step:changed', step => {

      this.evts.subscribe('step:back', () => {
        //Do something if back
        console.log('Back pressed: ', this.currentStep);
      });      //Handle the current step if you need
      this.currentStep = step;
      //Set the step condition to the default value
      this.stepCondition = this.stepDefaultCondition;
    });
    this.evts.subscribe('step:next', () => {
      //Do something if next
      console.log('Next pressed: ', this.currentStep);
    });
    this.loading.Show();
    this.is_charge_info = false
    console.log(this.is_charge_info)
    this.storage.get('people_id').then(data => {
      this.quizprovider.getquiz(data).subscribe(data => {
        this.response = data
        if (this.response.data.state == 1) {
          this.is_created = true
          this.quiz = this.response.data.questions.quiz
          this.questions = this.response.data.questions.quiz_questions
          this.questions.answer = 1


        } else if (this.response.data.state == 2) {
          this.questions = this.response.data.questions
          this.is_created = false
        }
        this.loading.Hide();
        this.is_charge_info = true;
        console.log(this.is_charge_info)

      });
    }, error => {
      this.loading.Hide();

    });

    console.log( this.myInput)


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizPage');
  }

  toggle() {
    this.stepCondition = true;
  }

  toggle2() {
    this.stepCondition = !this.stepCondition;
  }

  SendQuiz() {
    this.storage.get('people_id').then(data => {
      console.log(this.questions)
      if (this.response.data.state == 1) {
        this.quizprovider.updateansweruser(this.questions, data).subscribe(data => {
          this.navCtrl.setRoot(TrainerPage)
        })
      } else if (this.response.data.state == 2) {
        this.quizprovider.saveansweruser(this.questions, data).subscribe(data => {
        })
      }
    })

  }
  resize() {
    this.myInput.nativeElement.style.height = this.myInput.nativeElement.scrollHeight + 'px';
  }
  CalcularRow(value){
    console.log("hola")
    console.log(value)
    console.log("Wdafs")
    return Math.ceil(value  / this.text_area_quantity_space)
  }
  


}



// WEBPACK FOOTER //
// ./src/pages/quiz/quiz.ts