import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizPage } from './quiz';
import { IonSimpleWizard } from '../ion-simple-wizard/ion-simple-wizard.component';
import { IonSimpleWizardStep } from '../ion-simple-wizard/ion-simple-wizard.step.component';


@NgModule({
  declarations: [
    // QuizPage,

  ],
  imports: [
    IonicPageModule.forChild(QuizPage),
  ],
})
export class QuizPageModule {}
