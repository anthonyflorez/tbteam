import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TrainerProvider, LoadingProvider } from '../../providers';
import { ParseError } from '../../../node_modules/@angular/compiler';
import { TrainerPage } from '../trainer/trainer';

/**
 * Generated class for the DetailtrainerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailtrainer',
  templateUrl: 'detailtrainer.html',
})
export class DetailtrainerPage {
  response:any
  response2:any
  items:{}
  id:string
  dataTrainer:{
    dataitems:any,
    id:string
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public trainerprovider:TrainerProvider, private loading:LoadingProvider) {
    this.dataTrainer={
      dataitems:[],
      id:''
    }
    this.id=navParams.data.id
    this.loading.Show();
    this.trainerprovider.getTrainer(this.id).subscribe(data=>{
      this.response=data;
      this.items=this.response.data.trainer
      this.loading.Hide();
    }, error=>{
      this.loading.Hide();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailtrainerPage');
  }
  SendTrainer(){
    this.dataTrainer={
      dataitems:this.items,
      id:this.id
    }
    this.loading.Show();
    this.trainerprovider.saveChangeTrainer(this.dataTrainer).subscribe(data=>{
      this.response2=data;
      if(this.response2.success){
        this.navCtrl.setRoot('TrainerPage')
      }
      this.loading.Hide();

    },error=>{
      this.loading.Hide();
    });

  }
  isObject(x) {
    return x != null && typeof x === 'object';
  }

}
