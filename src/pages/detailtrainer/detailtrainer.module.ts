import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailtrainerPage } from './detailtrainer';

@NgModule({
  declarations: [
    DetailtrainerPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailtrainerPage),
  ],
})
export class DetailtrainerPageModule {}
