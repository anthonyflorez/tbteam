import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { HttpModule } from "@angular/http";

import { SignupPage } from './signup';
import { DetectFocusDirective } from "../../directives/detect-focus/detect-focus";
import { BrowserAnimationsModule } from '../../../node_modules/@angular/platform-browser/animations';


@NgModule({
  declarations: [
    // SignupPage,
    DetectFocusDirective,    

  ],
  imports: [
    // IonicPageModule.forChild(SignupPage),
    TranslateModule.forChild(),
    HttpModule,




  ],
  exports: [
    // SignupPage
  ],
  entryComponents:[
  
  ]
})
export class SignupPageModule { }
