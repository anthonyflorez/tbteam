import { Directive, ElementRef, Input, HostListener, Renderer2, ViewChild } from '@angular/core';

import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController,Events, ToastController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NumberValidator } from "../../validators/number";
import { EmailValidator } from "../../validators/email";
import { ValidateRpassword } from "../../validators/rpassword";

import { User } from '../../providers';
import { MainPage } from '../';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { ExampleDirective } from "../../directives/example/example";
import { VALID } from '../../../node_modules/@angular/forms/src/model';
import { IonSimpleWizardStep } from '../ion-simple-wizard/ion-simple-wizard.step.component';
import { BrowserAnimationsModule } from '../../../node_modules/@angular/platform-browser/animations';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  public response: any;
  registeredForm: FormGroup;
  public countries: any;
  public date_entry: any;
  public current_date: any;
  public section_parents: boolean;


  account: { document_number: string, name: string, last_name, email: string, password: string, phone: string, birthdate: string, gender: string, country: string } = {
    document_number: '',
    name: '',
    last_name: '',
    email: '',
    password: '',
    phone: "",
    birthdate: "",
    gender: "",
    country: ""

  };

   





  // Our translated text strings
  private signupErrorString: string;
  submitAttempt: boolean = false;
  step: any;
  stepCondition: any;
  stepDefaultCondition: any;
  currentStep: any;
  is_step:boolean=false;


  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, private renderer: Renderer2, private el: ElementRef,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,public evts: Events) {
    // this.renderer.addClass(this.valid.nativeElement, "my-class");

      /**
     * Step Wizard Settings
     */
    this.step = 1;//The value of the first step, always 1
    this.stepCondition = false;//Set to true if you don't need condition in every step
    this.stepDefaultCondition = this.stepCondition;//Save the default condition for every step
    //You can subscribe to the Event 'step:changed' to handle the current step
    this.evts.subscribe('step:changed', step => {

    this.evts.subscribe('step:back', () => {
      //Do something if back
      console.log('Back pressed: ', this.currentStep);
    });      //Handle the current step if you need
      this.currentStep = step;
      //Set the step condition to the default value
      this.stepCondition = this.stepDefaultCondition;
    });
    this.evts.subscribe('step:next', () => {
      //Do something if next
      console.log('Next pressed: ', this.currentStep);
    });
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    this.user.countries().subscribe(data => {
      this.response = data;
      this.countries = this.response.data.countries;
    })

    this.registeredForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
      last_name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
      document_number: ['', Validators.compose([ Validators.minLength(1), Validators.maxLength(40),Validators.required]), NumberValidator.checkNumber],
      birthdate: ['', Validators.required],
      country_id: ['', Validators.required],
      cellphone: ['', Validators.compose([Validators.minLength(7), Validators.maxLength(15), Validators.required, Validators.pattern('[0-9]*')])],
      phone: ['', Validators.compose([Validators.minLength(7), Validators.maxLength(15), Validators.required, Validators.pattern('[0-9]*')])],
      address: ['', Validators.required],
      email: ['', Validators.required, EmailValidator.mailFormat],
      origin_city: ['', Validators.required],
      country_origin_id:['', Validators.required],
      username: ['',Validators.required],
      gender: ['', Validators.required],
      facebook: [],
      instagram: [],
      password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
      rpassword: ['', Validators.compose([Validators.required])],
      name_parent: ['',Validators.compose([Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
      last_name_parent:['',Validators.compose([Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
      document_number_parent: [],
      cellphone_parent: ['', Validators.compose([Validators.minLength(7), Validators.maxLength(15), Validators.pattern('[0-9]*')])],
      phone_parent: ['', Validators.compose([Validators.minLength(7), Validators.maxLength(15), Validators.pattern('[0-9]*')])],
      email_parent: [''],
      origin_city_parent: [''],
      country_origin_id_parent:[''],
      address_parent:[''],
      club: [],
      category: ['-1', Validators.required],
      style: ['-1', Validators.required],
    }, {
        validator: ValidateRpassword.checkPassword
      })

  }

  calcularEdad(date_entry) {
    var year = date_entry.year;
    var month = date_entry.month - 1;
    var day = date_entry.day;
    this.date_entry = new Date(year, month, day, 0, 0, 0, 0).getTime();
    this.current_date = new Date().getTime();
    var result = this.current_date - this.date_entry;

    if (this.convertmili(result) >= 18) {
      this.section_parents = true;
    } else {
      this.section_parents = false;
    }
  }

  convertmili(mSeconds) {
    var checkYear = Math.floor(mSeconds / 31536000000);
    return checkYear;
  }
  CreateUser() {
    console.log(this.registeredForm.value)
    this.submitAttempt = true;
    console.log(this.registeredForm)
    if (this.registeredForm.valid == true) {
      // Attempt to login in through our User service
      this.user.signup(this.registeredForm.value).subscribe((resp) => {
        this.navCtrl.push(MainPage);
      }, (err) => {
        this.navCtrl.push(SignupPage);
        // Unable to sign up
        let toast = this.toastCtrl.create({
          message: this.signupErrorString,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      });
    }else{
      alert("error en datos");
    }

  }

  doSignup() {

  }

  validateDocument(document) {
    this.user.validatedocument(document).subscribe(data => {
      // this.renderer.addClass(this.myButton.nativeElement, "my-class");
      this.response = data;
      if (this.response.data.exist) {
        this.registeredForm.controls['document_number'].setErrors({ 'incorrect': true });
        this.renderer.addClass(this.el.nativeElement, 'invalid');
        this.renderer.removeClass(this.el.nativeElement, 'ng-valid');
        this.renderer.addClass(this.el.nativeElement, 'ng-invalid');
      } else {
        this.renderer.addClass(this.el.nativeElement, 'ng-valid');
        this.renderer.removeClass(this.el.nativeElement, 'invalid');
      }
    })
  }
  validateEmail(email) {

    this.user.validateemail(email).subscribe(data => {
      // this.renderer.addClass(this.myButton.nativeElement, "my-class");
      this.response = data;
      if (this.response.data.exist) {
        this.registeredForm.controls['email'].setErrors({ 'incorrect': true });
        this.renderer.addClass(this.el.nativeElement, 'invalid');
        this.renderer.removeClass(this.el.nativeElement, 'ng-valid');
        this.renderer.addClass(this.el.nativeElement, 'ng-invalid');
      } else {
        this.renderer.addClass(this.el.nativeElement, 'ng-valid');
        this.renderer.removeClass(this.el.nativeElement, 'invalid');
      }
    })
  }
  validateUsername(username) {
    this.user.validateusername(username).subscribe(data => {
      // this.renderer.addClass(this.myButton.nativeElement, "my-class");
      this.response = data;
      if (this.response.data.exist) {
        this.registeredForm.controls['username'].setErrors({ 'incorrect': true });
        this.renderer.addClass(this.el.nativeElement, 'invalid');
        this.renderer.removeClass(this.el.nativeElement, 'ng-valid');
        this.renderer.addClass(this.el.nativeElement, 'ng-invalid');
      } else {
        this.renderer.addClass(this.el.nativeElement, 'ng-valid');
        this.renderer.removeClass(this.el.nativeElement, 'invalid');
      }
    })
  }

  portChange(event: { component: SelectSearchableComponent, value: any }) {
    console.log('port:', event.value);
  }
    /**
   * Demo functions
   */
  onFinish() {

    alert("dfgsdf")
  }

  toggle() {
    console.log(this.registeredForm.controls)
    let name=this.registeredForm.controls.name.valid
    let last_name=this.registeredForm.controls.last_name.valid
    let document_number=this.registeredForm.controls.document_number.valid
    let birthdate=this.registeredForm.controls.birthdate.valid
    let country_id=this.registeredForm.controls.country_id.valid
    let cellphone=this.registeredForm.controls.cellphone.valid
    let phone=this.registeredForm.controls.phone.valid
    let address=this.registeredForm.controls.address.valid
    let username=this.registeredForm.controls.username.valid
    let email=this.registeredForm.controls.email.valid
    let origin_city=this.registeredForm.controls.origin_city.valid
    let country_origin_id=this.registeredForm.controls.country_origin_id.valid
    let gender=this.registeredForm.controls.gender.valid
    let password=this.registeredForm.controls.password.valid
    let rpassword=this.registeredForm.controls.rpassword.valid   
    if(name && last_name && document_number
       &&  birthdate && country_id && cellphone
       && phone &&  address && username && email && origin_city 
       && country_origin_id &&  gender && password && rpassword){
      this.stepCondition = !this.stepCondition;
      this.is_step=true;
    }else{
      this.is_step=false
    }

  }

  toggle2() {

      this.stepCondition = !this.stepCondition;
  }

  getIconStep2() {
    return this.stepCondition ? 'unlock' : 'lock';
  }

  getIconStep3() {
    return this.stepCondition ? 'happy' : 'sad';
  }
  getLikeIcon() {
    return this.stepCondition ? 'thumbs-down' : 'thumbs-up';
  }


  textChange(e) {
    if (e.target.value && e.target.value.trim() !== '') {
      this.stepCondition = true;
    } else {
      this.stepCondition = false;
    }
  }
}
