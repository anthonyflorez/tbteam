import { NgModule, LOCALE_ID } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainerPage } from './trainer';
import * as moment from 'moment';
import { NgCalendarModule  } from 'ionic2-calendar';
import localeEs from '@angular/common/locales/es-US';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeEs);


@NgModule({
  declarations: [
    TrainerPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainerPage),
    NgCalendarModule,
  ],
  exports: [
    TrainerPage
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-US' }
  ]
})
export class TrainerPageModule {}
