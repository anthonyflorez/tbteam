import { Component, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { TrainerProvider, LoadingProvider } from '../../providers'
import { Storage } from '@ionic/storage';
import { DetailtrainerPage } from '../detailtrainer/detailtrainer';
import { Network } from '@ionic-native/network';



/**
 * Generated class for the TrainerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainer',
  templateUrl: 'trainer.html',
})
export class TrainerPage {
  @ViewChild("myButton") myButton: ElementRef;
  eventSource;
  viewTitle;
  isToday: boolean;
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    locale: 'es-US'
  }; // these are the variable used by the calendar.
  events: any;
  events2: any = [];
  events3: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private elemnt: ElementRef,
    public renderer: Renderer2, public trainerprovider: TrainerProvider, private storage: Storage, private platform: Platform, private network: Network,
    private loading: LoadingProvider) {


    var item = this.elemnt.nativeElement.querySelector('div');

    console.log(item)
    this.loading.Show();
    this.storage.get('people_id').then(data => {
      this.trainerprovider.getEvents(data).subscribe(data => {
        this.loading.Hide();
        console.log(data)
        this.events = data;
        this.events.forEach(element => {
          let event = {
            allDay: false,
            endTime: new Date(element.end),
            startTime: new Date(element.start),
            title: element.title,
            eventColor: element.color,
            id: element.id
          }
          this.events2.push(event);
        });
        console.log(this.events2)
        this.eventSource = this.events2;
      }, error => {
        this.loading.Hide();
        console.log(error)
      })
    });
  }


  loadEvents() {
    this.eventSource = this.events3;


  }
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
  onEventSelected(event) {
    console.log(this.events3)
    console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
  }
  changeMode(mode) {
    this.calendar.mode = mode;
  }
  today() {
    this.calendar.currentDate = new Date();
  }
  onTimeSelected(ev) {
    var events = []
    const day = 86400000;
    let date_botom = ev.selectedTime.getTime();
    let date_up = ev.selectedTime.getTime() + day;

    this.events2.forEach(element => {
      let dayevent = element.startTime.getTime();

      if (date_botom <= dayevent && date_up >= dayevent) {
        events.push(element)
      }
    });
    this.events3 = events
    console.log(this.events3)
  }

  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }
  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }
  viewDetail(event) {
    this.navCtrl.push(DetailtrainerPage, event)
  }

  onRangeChanged(ev) {
    console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
  }
  markDisabled = (date: Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return date < current;
  };
}
