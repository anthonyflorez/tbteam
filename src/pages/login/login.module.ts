import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from '../signup/signup';

import { LoginPage } from './login';

@NgModule({
  declarations: [
     LoginPage,
    // SignupPage
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    TranslateModule.forChild()
  ],
  exports: [
    LoginPage
  ]
  , 
   entryComponents: [
      LoginPage
  ]
})
export class LoginPageModule { }