import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User, LoadingProvider } from '../../providers';
import { MainPage } from '../';
import { WelcomePage } from '../welcome/welcome';
import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';
import { TrainerPage } from '../trainer/trainer';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  public response:any;
  account: { username: string, password: string } = {
    username: '',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService, public storage:Storage, private loading:LoadingProvider) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doLogin() {

    this.user.login(this.account).subscribe((resp) => {
      this.response=resp;
      if(this.response.success==true){
        this.storage.set('user_id', this.response.data.user_id);
        this.storage.set('people_id', this.response.data.people_id);
        this.storage.ready().then(data=>{
          this.storage.get('people_id').then((val) => {
            console.log('people_id', val);
          });

        })
        this.navCtrl.setRoot(TrainerPage);
      }else{
        this.navCtrl.setRoot(LoginPage);
      }
    }, (err) => {
      this.loading.Hide();
      this.navCtrl.push(LoginPage);
      // Unable to log in
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();

    });
  }
  signup() {
    this.navCtrl.push(SignupPage)
  }
}
