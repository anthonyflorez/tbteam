import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizsPage } from './quizs';

@NgModule({
  declarations: [
    QuizsPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizsPage),
  ],
})
export class QuizsPageModule {}
