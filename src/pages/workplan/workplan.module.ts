import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { WorkPlanPage } from './workplan';


@NgModule({
  declarations: [
    WorkPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkPlanPage),
    TranslateModule.forChild()
  ],
  exports: [
    WorkPlanPage
  ]
})
export class WorkPlanPageModule { }
