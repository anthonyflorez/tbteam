import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Globals } from '../../app/globals';


import { Plans } from '../../providers';


@IonicPage()
@Component({
    selector: 'page-plan-detail',
    templateUrl: 'workplan.html'
})
export class WorkPlanPage {



    plan: any;
    data_plan: any;
    id_plan: string;
    constructor(public navCtrl: NavController, private navParams: NavParams, plans: Plans, public globals: Globals, ) {
        this.plan = this.navParams.data.plan || plans.defaultPlan;

  
    }



}



