import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../providers';
import { style } from '@angular/core/src/animation/dsl';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NumberValidator } from "../../validators/number";
import { EmailValidator } from "../../validators/email";
import { ValidateRpassword } from "../../validators/rpassword";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})

export class ProfilePage {
  person: {}
  registeredForm: FormGroup;
  countries: any;
  response: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public user: User, public formBuilder: FormBuilder, public storage: Storage) {
    this.user.countries().subscribe(data => {
      this.response = data;
      this.countries = this.response.data.countries;
      console.log(this.countries)
    })
    this.storage.get('people_id').then(data => {
      this.user.getUser(data).subscribe(data => {
        console.log(data)
        this.response = data
        this.person = this.response.data
      })
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  changecountry(country_id) {
    console.log(country_id)
  }
  changeCategory(category_id) {
    console.log(category_id)
  }
  UpdateUser() {
    console.log(this.person)
  }
}
