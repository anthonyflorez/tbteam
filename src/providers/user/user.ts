import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';
import { AlertProvider } from '../alert/alert'

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _user: any;

  constructor(public api: Api, public alert: AlertProvider) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  //   Login(){

  //     if(this.dataUser.username === null || this.dataUser.password === null)
  //     {
  //       return Observable.throw("Please insert credemtials");
  //     }else
  //     {
  //       return Observable.create(observer => {
  //         let headers = new Headers();
  //         headers.append('Content-Type','application/json');
  //         headers.append('Accept','application/json');
  //         return new Promise(resolve => {
  //           this.http.post('http://demos2.obvio.com.co/tbteamswim/users/login.json', this.dataUser, headers)
  //             .map(res => res.json())
  //             .subscribe(data => {
  //               this.data = data;
  //               // this.storage.clear();
  //             //   this.storage.remove('nameUser')
  //             //   this.storage.remove('idUser')
  //             //   this.storage.remove('dataUser')
  //             //   this.storage.set('nameUser',data.user.nombre);
  //             //   this.storage.set('idUser',data.user.idUsuario);
  //             //   this.storage.set('dataUser',dataUser);
  //               observer.next(data);
  //               observer.complete();
  //               resolve(this.data);
  //             })
  //         })
  //       });
  //     }
  //  }
  login(accountInfo: any) {
    let seq = this.api.post('users/loginApp.json', accountInfo).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in

      console.log(res);
      if (res.success == true) {
        this._loggedIn(res);
      } else {
        this.alert.presentAlert('Error', 'error en el usuario o contraseña', 'aceptar')
      }
    }, err => {

      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.post('users/addApp.json', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }


  getUser(data){
    console.log(data)
    let seq = this.api.post('users/getUser.json', {id:5}).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }

  countries() {
    let seq = this.api.get('users/countries.json').share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  validatedocument(data) {
    let seq = this.api.post('users/validatedocument.json', { document_number: data }).share();
    seq.subscribe((res: any) => {
      if (res.status == 'success') {
      }
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }
  validateemail(data) {
    let seq = this.api.post('users/validateemail.json', { email: data }).share();
    seq.subscribe((res: any) => {
      if (res.status == 'success') {
      }
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }

  validateusername(data) {
    let seq = this.api.post('users/validateusername.json', { username: data }).share();
    seq.subscribe((res: any) => {
      if (res.status == 'success') {
      }
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user = resp.user;
  }
}
