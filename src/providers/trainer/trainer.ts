import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api'

/*
  Generated class for the TrainerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TrainerProvider {

  constructor(public http: HttpClient, public api:Api) {
    console.log('Hello TrainerProvider Provider');
  }

  getEvents(id) {
    let seq = this.api.post('trainers/getalltrainer.json', {person:id}).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }

  saveChangeTrainer(data){
    console.log(data)
    let seq = this.api.post('trainers/savechangetrainer.json', data).share();
    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }

  getTrainer(id) {
    let seq = this.api.post('trainers/mytrainerApp.json', {id:id}).share();
    seq.subscribe((res: any) => {
    }, err => {
    
      console.error('ERROR', err);
    });

    return seq;
  }

}
