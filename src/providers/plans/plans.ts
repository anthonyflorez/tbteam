import { Injectable } from '@angular/core';

import { Plan } from '../../models/plan';
import { Api } from '../api/api';

@Injectable()
export class Plans {

  constructor(public api: Api) { }

  query(params?: any) {
    return this.api.get('/plans', params);
  }

  add(item: Plan) {
  }

  delete(item: Plan) {
  }

}
