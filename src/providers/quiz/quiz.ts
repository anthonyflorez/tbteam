import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';

/*
  Generated class for the QuizProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QuizProvider {

  constructor(public http: HttpClient, public api:Api) {
    console.log('Hello QuizProvider Provider');
  }
  getquiz(id) {
    let seq = this.api.post('quiz/quizsuser.json',{id:id}).share();
    seq.subscribe((res: any) => {
      if (res.status == 'success') {
      }
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }
  saveansweruser(data,id) {
    console.log(data);
    let seq = this.api.post('quiz/saveansweruser.json', {data,id:id}).share();
    seq.subscribe((res: any) => {
      if (res.status == 'success') {
      }
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }
  updateansweruser(data,id) {
    console.log(data);
    let seq = this.api.post('quiz/updateanweruser.json', {data,id:id}).share();
    seq.subscribe((res: any) => {
      if (res.status == 'success') {
      }
    }, err => {
      console.error('ERROR', err);
    });
    return seq;
  }

}



// WEBPACK FOOTER //
// ./src/providers/quiz/quiz.ts